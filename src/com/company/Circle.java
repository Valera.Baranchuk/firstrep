package com.company;
import static java.lang.Math.PI;

public class Circle extends Shape{
    private int r;
    public Circle(int x0, int y0, int r) {
        super(x0, y0);
        this.r = r;
    }
    @Override
    public double area() {
        return PI * r * r;
    }
    public void print (){
        System.out.println("r = " + r + " " + getX0());
    }
}
