package com.company;

abstract public class Shape {
    private int x0, y0;

    public int getX0() {
        return x0;
    }

    public int getY0() {
        return y0;
    }

    public void setX0(int x) {
        x0 = x;
    }

    public void setY0(int y) {
        y0 = y;
    }
    public void move (int a, int b){
        setX0(getX0() + a);
        setY0(getY0() + b);
    }

    public Shape(int x0, int y0) {
        setX0(x0);
        setY0(y0);
    }
    public void print (){}

    abstract public double area();
}

