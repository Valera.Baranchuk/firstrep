package com.company;

public class Rect extends Shape {
    private int a, b;

    public Rect(int x0, int y0, int a, int b) {
        super(x0, y0);
        this.a = a;
        this.b = b;
    }
    @Override
    public double area (){
        return (double) a*b;
    }
    public void print(){
        System.out.println("a = " + a + " b = " + b + " "+ getX0());
    }

}

